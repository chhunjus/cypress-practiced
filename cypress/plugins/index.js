/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

module.exports = (on, config) => {
  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config
}

/**
 * @type {Cypress.PluginConfig}
 */
// eslint-disable-next-line no-unused-vars
// const { Pool } = require('pg')
// const dbConfig = require('../../cypress/fixtures/dbConfig.json');

// module.exports = (on, config) => {
//   // `on` is used to hook into various events Cypress emits
//   // `config` is the resolved Cypress config
//   on("task", {
//     noParam,
//     singleParam: singleParam,
//     multipleParam: multipleParam,
//     mySQLgetDBDataAsync: mySQLgetDBDataAsync,
//     pgSqldbQuery: pgSqldbQuery,
//   });
//   return config;
// };

// //task event with no parameter
// function noParam(){
//   console.log('NO param task');
//   return null;
// }

// // With Param - Single Arg
// function singleParam(message) {
//   console.log(message);
//   return "OK";
// }

// // With Param - Multiple Args
// function multipleParam(obj) {
//   console.log(`Hello ${obj.name}, you are ${obj.age} years old`);
//   return "OK";
// }

// //Connects to mysql DB, sends query to database, the query is sent from testfile.
// function mySQLgetDBDataAsync() {
//   let con = mysql.createConnection(dbConfig.db);
//   return new Promise((resolve, reject) => {
//     con.connect(function(err) {
//       if (err) throw err;
//       con.query(query, (err, result) => {
//         con.end();
//         if (err) {
//           reject(err);
//         } else {
//           resolve(result);
//         }
//       });
//     });
//   });
// }

// //Connects to pgsql DB, sends query to database, the query is sent from testfile.
// function pgSqldbQuery(query) {
//   const pool = new Pool(dbConfig.db)
//   return new Promise((resolve, reject) => {
//     pool.connect(function(err){
//       if (err) throw err;
//       console.log("connected to db")
//       pool.query(query, (err, res) => {
//         pool.end() 
//         if(err){
//           reject(err)
//         }
//         else{
//           resolve(res)
//         }
//     })
//   })
// })
// }